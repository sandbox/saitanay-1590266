<?php

/**
 * @file
 * Author: Tanay Sai
 * Article Spinner Module: Contains the Admin Config Form for the module
 */

/**
 * Build the settings form.
 */
function article_spinner_settings_form() {
  $query = db_select('node_type', 'nt');
  $query->fields('nt', array('type', 'name'));
  $result = $query->execute();
  $form = array();
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('ArticleSpinner.in Username'),
    '#default_value' => variable_get('article_spinner_username', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('ArticleSpinner.in Password'),
    '#default_value' => variable_get('article_spinner_password', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $spin_options = array(
    0 => 'Default : Best Spin [Recommended]',
    1 => 'Random Spin : Use this when you need more than 1 spinned article from the same original article',
    2 => 'Double Spin : More Spin ',
    3 => 'Triple Spin : Very unique content. But English may be Awkward');

  $form['spinlevel'] = array(
    '#type' => 'radios',
    '#title' => t('Spin Level'),
    '#default_value' => variable_get('article_spinner_spinlevel', ''),
    '#options' => $spin_options,
  );
  $form['spintitles'] = array(
    '#type' => 'checkbox',
    '#title' => t('Spin Title'),
    '#attributes' => variable_get('article_spinner_spintitles', 0) == 1 ? array('checked' => 'checked') : array(),
  );
  $form['spinsummaries'] = array(
    '#type' => 'checkbox',
    '#title' => t('Spin Summary'),
    '#attributes' => variable_get('article_spinner_spinsummaries', 0) == 1 ? array('checked' => 'checked') : array(),
  );
  $form['spineditmode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Spin while editing the node also'),
    '#attributes' => variable_get('article_spinner_spineditmode', 0) == 1 ? array('checked' => 'checked') : array(),
    '#description' => t('Enable this if you wish to spin the node when you 
                        edit the article. Disabling the option will spin the 
                        node only once at the time of creation.'),
  );

  $content_types = variable_get('article_spinner_contenttypes', array());
  $form['description'] = array('#markup' => '<b>Select the content types that should be spinned</b>');
  foreach ($result as $record) {
    $form['article_spinner-ct-' . $record->type] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($record->name),
      '#attributes' => in_array($record->type, $content_types) == 1 ? array('checked' => 'checked') : array(),
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Spinner settings'),
  );
  return $form;
}

/**
 * Submit handler for the configuration form.
 *
 * @param array $form
 *   The form structure.
 * @param array $form_state
 *   The submitted values from the form.
 */
function article_spinner_settings_form_submit($form, &$form_state) {
  $article_spinner_content_types_array = array();
  foreach ($form_state['values'] as $key => $value) {
    if (strstr($key, 'article_spinner-ct-')) {
      if ($value == 1) {
        /*
         * Not using Drupals drupal_substr() as
         * http://api.drupal.org/api/drupal/includes!unicode.inc/
         * function/drupal_substr/7 says
         *    "Note that for cutting off a string at a known
         *     character/substring location, the usage of PHP's normal
         *     strpos/substr is safe and much faster."
         */
        $article_spinner_content_types_array[] = substr($key, 19);
      }
    }
  }
  variable_set('article_spinner_username', $form_state['values']['username']);
  variable_set('article_spinner_password', $form_state['values']['password']);
  variable_set('article_spinner_spinlevel', $form_state['values']['spinlevel']);
  variable_set('article_spinner_spintitles', $form_state['values']['spintitles']);
  variable_set('article_spinner_spinsummaries', $form_state['values']['spinsummaries']);
  variable_set('article_spinner_spineditmode', $form_state['values']['spineditmode']);
  variable_set('article_spinner_contenttypes', $article_spinner_content_types_array);
  drupal_set_message(t("Your settings have been saved."), 'status');
}
